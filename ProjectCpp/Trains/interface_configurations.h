#pragma once

#include <vector>
#include "ScheduleGenerator.h"
#include "BruteForceGenerator.h"
#include "ConfigurationCalculator.h"

using namespace std;
/** Returns the required running time. 

    In dependece of the specified quantile the method returns the
    maximal running time of the fatest quantile*100 percent of
    schedules.

 @param haedway_matrix Headway matrix. The value of the i-th row und j-th column is the headway time if train type j follows train type i.

 @param running_times Running time for each train type.

 @param train_type_mixture Nummber of trains for each train type, for instance 2 freigt trains and 1 inter city express.
 
 @param time Available time

 @param quantile Considered quantile ( Value between 0 and 1 ). 0 means best schedule, 1 meas worst schedule.

**/

double configuration(const vector < vector< double >>& headway_matrix,
    const vector< int >& running_times,
    const vector< int >& train_type_mixture,
    int time,
    double quantile)
{
    TrainsInputData* data = new TrainsInputData();

    data->Alpha = quantile;
    
    data->RunningTimes = new uint[running_times.size() + 1];
    data->RunningTimes[0] = 0;
    for (vector<int>::size_type i = 0; i != running_times.size(); i++) 
    {
        data->RunningTimes[i + 1] = running_times[i + 1];
    }

    data->NumberOfTypes = train_type_mixture.size();

    int n = data->NumberOfTypes + 1;

    data->NumberOfTrainsPerType = new uint[n];
    data->NumberOfTrainsPerType[0] = 1;
    for (vector<int>::size_type i = 0; i != train_type_mixture.size(); i++)
    {
        data->NumberOfTrainsPerType[i + 1] = train_type_mixture[i + 1];
    }

    data->TrainDelays = new double[n*n];
    for (int i = 0; i < n; i++)
    {
        // pad trains delays with 0 row and 0 column for type 0
        data->TrainDelays[i] = 0;
        data->TrainDelays[i*n] = 0;
    }

    for (vector<vector<double>>::size_type i = 0; i != headway_matrix.size(); i++)
    {
        for (vector<double>::size_type j = 0; j != headway_matrix[i].size(); i++)
        {
            data->TrainDelays[i*n + j] = headway_matrix[i][j];
        }
    }

    return CalculateConfiguration(data);

}

