#include "DataReader.h"
#include "pugixml.hpp"
#include <map>

using namespace std;
using namespace pugi;

DataReader::DataReader(char* filePath)
{
    Data = new vector<TrainsInputData*>();

    xml_document* doc = new xml_document();
    xml_parse_result result = doc->load_file(filePath);

    for (xml_node track: doc->root().child("infrastructure").child("tracks").children("track"))
    {
        TrainsInputData* data = new TrainsInputData();
        map<char_t, uint>* trainTypeMapping = new map<char_t, uint>();

        vector<uint>* runningTimes = new vector<uint>();

        uint i = 0;

        for (xml_node drivetime: track.children("drivetime"))
        {
            (*trainTypeMapping)[*drivetime.attribute("traintypeID").as_string()] = i;
            runningTimes->push_back(drivetime.attribute("value").as_uint());
        }

        data->NumberOfTypes = runningTimes->size();
        uint n = data->NumberOfTypes + 1;
        data->RunningTimes = new uint[data->NumberOfTypes+1];
        data->RunningTimes[0] = 0;

        for (uint i=0; i< data->NumberOfTypes; i++)
        {
            data->RunningTimes[i+1] = (*runningTimes)[i];
        }

        data->TrainDelays = new double[n*n];

        // TODO 0 row and column

        // <headway traintypeID_preceded="GV" trackID_preceded="1-2" traintypeID_succeded="GV" trackID_succeded="1-2" value="5"/>
        for (xml_node headway: track.children("headway"))
        {
            uint preceded = (*trainTypeMapping)[headway.attribute("traintypeID_preceded").as_uint()];
            uint succeded = (*trainTypeMapping)[headway.attribute("traintypeID_succeded").as_uint()];
            data->TrainDelays[preceded + succeded * n] = headway.attribute("value").as_double();
        }

        Data->push_back(data);

        delete runningTimes;
        delete trainTypeMapping;
    }

    delete doc;
}

DataReader::~DataReader(void)
{
}
