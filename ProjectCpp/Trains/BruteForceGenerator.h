#pragma once
#include "schedulegenerator.h"
#include <vector>
#include <queue>

using namespace std;

class MatrixConstructionInfo
{
public:
    int* Matrix;
    uint LastGeneratedElementIndex;
    uint* RemainingRowNumberOfTrainsPerType;
    uint* RemainingColumnNumberOfTrainsPerType;

    MatrixConstructionInfo()
    {
    }

    MatrixConstructionInfo(MatrixConstructionInfo* info, int n)
    {
        LastGeneratedElementIndex = info->LastGeneratedElementIndex + 1;
        Matrix = CopyArrayPart(info->Matrix, info->LastGeneratedElementIndex + 2, info->LastGeneratedElementIndex + 1);
        RemainingColumnNumberOfTrainsPerType = CopyArray(info->RemainingColumnNumberOfTrainsPerType, n);
        RemainingRowNumberOfTrainsPerType = CopyArray(info->RemainingRowNumberOfTrainsPerType, n);
    }

    ~MatrixConstructionInfo()
    {
        delete[] RemainingColumnNumberOfTrainsPerType;
        delete[] RemainingRowNumberOfTrainsPerType;
        delete[] Matrix;
    }

    uint* MatrixConstructionInfo::CopyArray(uint* arr, uint size)
    {
        uint* result = new uint[size];

        for (uint i = 0; i < size; i++)
        {
            result[i] = arr[i];
        }

        return result;
    }

    int* MatrixConstructionInfo::CopyArrayPart(int* arr, uint outSize, uint inSize)
    {
        int* result = new int[outSize];

        for (uint i = 0; i < inSize; i++)
        {
            result[i] = arr[i];
        }

        return result;
    }
};

class BruteForceGenerator :
    public ScheduleGenerator
{
private:
    vector<int*>* _matrices;
    queue<MatrixConstructionInfo*>* _constructionQueue;
    uint _lastGeneratedScheduleIndex;
    uint* CopyArray(uint* arr, uint size);
    int* BruteForceGenerator::CopyArrayPart(int* arr, uint outSize, uint inSize);

public:
    BruteForceGenerator(TrainsInputData* data);
    ~BruteForceGenerator(void);

    virtual Schedule* Next();
};

