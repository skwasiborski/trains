#pragma once

#include <string>

#define ulong unsigned long
#define uint unsigned int

struct TrainsInputData
{
public:
    // alpha
    double Alpha;

    // t - number of train types
    uint NumberOfTypes;

    // r - padded with 0 at the beginning (0 type)
    uint* RunningTimes;

    // l - padded with 1 at the beginning (0 type)
    uint* NumberOfTrainsPerType;

    // m - padded with 0 column and row at the beginning (0 type)
    // TrainsDelays[i * n + j] delay if j goes after i
    double* TrainDelays;
};

// Graph representing equivalence class
class Schedule
{
private:
    int* _kirhoffMatrix;
    uint _kirhoffMatrixSize;
    uint _kirhoffMatrixSizeSq;

    uint* _mec;
    uint _mecLength;

    TrainsInputData* _data;

    void MinimalEulerCycle();

    // global variables for dfs
    uint* _dfsnum; // when each vertex is first visited
    uint _num; // globalcounter for dfsnum
    uint* _low; // smallest dfsnum reachable from the subtree

    uint MinimalNeighbour(uint u, uint* miu);
    uint MinimalNeighbourCore(uint u, uint* miu);

    uint Deg(uint i);
    ulong Factorial(uint i);
    uint CroutLUDecomposition(double *A, uint n);
    int Determinant(double *A, uint n);
    int Minor1Determinant();
    uint Deleta(uint i, uint j);
    ulong Choose(uint n, uint k);

    void runDFS(int u, int state[]);

public:
    Schedule(TrainsInputData* data, int KirhoffMatrix[], uint size);
    ~Schedule(void);

    std::string ToString();
    double RunningTime();
    int Determinant();
    uint Cardinality();
    bool IsConnected();
};
