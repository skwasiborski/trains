#include "BruteForceGenerator.h"

BruteForceGenerator::BruteForceGenerator(TrainsInputData* data)
    :ScheduleGenerator(data)
{
    uint  n = _data->NumberOfTypes + 1;
    uint nsq = n * n;

    _matrices = new vector<int*>();
    _constructionQueue = new queue<MatrixConstructionInfo*>();

    MatrixConstructionInfo* info = new MatrixConstructionInfo();
    info->LastGeneratedElementIndex = 0;
    info->Matrix = new int[1];
    info->Matrix[0] = 0;
    info->RemainingColumnNumberOfTrainsPerType = CopyArray(data->NumberOfTrainsPerType, n);
    info->RemainingRowNumberOfTrainsPerType = CopyArray(data->NumberOfTrainsPerType, n);
    _constructionQueue->push(info);

    while (!_constructionQueue->empty())
    {
        MatrixConstructionInfo* info = _constructionQueue->front();
        _constructionQueue->pop();

        if (info->LastGeneratedElementIndex + 1 < nsq)
        {
            uint row = (info->LastGeneratedElementIndex + 1) / n;
            uint col = (info->LastGeneratedElementIndex + 1) % n;
            uint max = info->RemainingColumnNumberOfTrainsPerType[col] < info->RemainingRowNumberOfTrainsPerType[row] ? 
                info->RemainingColumnNumberOfTrainsPerType[col] : info->RemainingRowNumberOfTrainsPerType[row];

            for (int i = 0; (uint)i <= max; i++)
            {
                MatrixConstructionInfo* newInfo = new MatrixConstructionInfo(info, n);

                newInfo->Matrix[info->LastGeneratedElementIndex + 1] = -i;
                newInfo->RemainingColumnNumberOfTrainsPerType[col] -= i;
                newInfo->RemainingRowNumberOfTrainsPerType[row] -= i;

                _constructionQueue->push(newInfo); 
            }
        }
        else
        {
            // we do not allow loops on type 0
            if (info->RemainingColumnNumberOfTrainsPerType[0] == 0 &&
                info->RemainingRowNumberOfTrainsPerType[0] == 0)
            {
                for (uint i=0; i < n; i++)
                {
                    info->Matrix[i + i*n] = data->NumberOfTrainsPerType[i];
                }

                _matrices->push_back(info->Matrix);
                info->Matrix = NULL;
            }
        }

        delete info;
    }
}

BruteForceGenerator::~BruteForceGenerator(void)
{
}

Schedule* BruteForceGenerator::Next()
{
    Schedule* newSchedule;

    do
    {
        if (_matrices->empty())
        {
            return nullptr;
        }

        newSchedule = new Schedule(_data, _matrices->back(), _data->NumberOfTypes+1);
        _matrices->pop_back();

    } while (!newSchedule->IsConnected());

    return newSchedule;
}

uint* BruteForceGenerator::CopyArray(uint* arr, uint size)
{
    uint* result = new uint[size];

    for (uint i = 0; i < size; i++)
    {
        result[i] = arr[i];
    }

    return result;
}

int* BruteForceGenerator::CopyArrayPart(int* arr, uint outSize, uint inSize)
{
    int* result = new int[outSize];

    for (uint i = 0; i < inSize; i++)
    {
        result[i] = arr[i];
    }

    return result;
}