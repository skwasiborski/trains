#pragma once
#include "Schedule.h"


class ScheduleGenerator
{
protected:
    TrainsInputData* _data;

public:
    ScheduleGenerator(TrainsInputData* data)
    {
        _data = data;
    }
    ~ScheduleGenerator(void) 
    {
    }

    virtual Schedule* Next() = 0;
};
