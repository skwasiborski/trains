#include "stdafx.h"
#include "Schedule.h"
#include "Assert.h"
#include "BruteForceGenerator.h"
#include "ConfigurationCalculator.h"
#include <iostream>

using namespace std;

void Given_SimpleMatrix_When_DeterminantCalculated_Then_ReturnedValueIsCorrect()
{
    cout << "Given_SimpleMatrix_When_DeterminantCalculated_Then_ReturnedValueIsCorrect" << endl;
    // Arrange
    int kirhoffMatrix[9] = 
    { -2,  1, 0, 
    0, -2, 1,
    1,  0, -2 };

    TrainsInputData* data = new TrainsInputData();

    Schedule* s = new Schedule(data, kirhoffMatrix, 3);

    // Act
    int result = s->Determinant();

    // Assert
    Assert::Equal(-7, result);
}

void Given_SimpleGraph_When_RunningTimeCalled_Then_ReturnedValueIsCorrect()
{
    cout << "Given_SimpleGraph_When_RunningTimeCalled_Then_ReturnedValueIsCorrect" << endl;
    // Arrange
    int kirhoffMatrix[9] = 
    { 1,  -1, 0, 
    0, 1, -1,
    -1,  0, 1 };

    TrainsInputData* data = new TrainsInputData();
    data->Alpha =0;
    data->NumberOfTypes = 2;
    uint runningTimes[3] = { 0, 1, 2 };
    data->RunningTimes = runningTimes;
    uint numberOfTrainsPerType[3] = { 1, 1, 1 };
    data->NumberOfTrainsPerType = numberOfTrainsPerType;
    double trainDelays[9] = 
    { 0, 0, 0, 
    0, 1, 2, 
    0, 2, 3 };
    data->TrainDelays = trainDelays;

    Schedule* s = new Schedule(data, kirhoffMatrix, 3);

    // Act
    double result = s->RunningTime();

    // Assert
    Assert::Equal(4, result);
}

void Given_NotSoSimpleGraph_When_RunningTimeCalled_Then_ReturnedValueIsCorrect()
{
    cout << "Given_NotSoSimpleGraph_When_RunningTimeCalled_Then_ReturnedValueIsCorrect" << endl;
    // Arrange
    int kirhoffMatrix[16] = 
    { 1, -1,  0,  0,
    0,  2, -1, -1,
    0, -1,  1,  0,
    -1,  0,  0,  1 };

    TrainsInputData* data = new TrainsInputData();
    data->Alpha =0;
    data->NumberOfTypes = 3;
    uint runningTimes[4] = { 0, 1, 2, 3 };
    data->RunningTimes = runningTimes;
    uint numberOfTrainsPerType[4] = { 1, 2, 1, 1 };
    data->NumberOfTrainsPerType = numberOfTrainsPerType;
    double trainDelays[16] = 
    { 0, 0, 0, 0, 
    0, 1, 2, 3,
    0, 2, 3, 4,
    0, 5, 6, 7 };

    data->TrainDelays = trainDelays;

    Schedule* s = new Schedule(data, kirhoffMatrix, 4);

    // Act
    double result = s->RunningTime();

    // Assert
    Assert::Equal(10, result);
}

void Given_SimpleInput_When_BrutForceGeneratorCalled_Then_AllGraphsAreGenerated()
{
    cout << "Given_SimpleInput_When_BrutForceGeneratorCalled_Then_AllGraphsAreGenerated" << endl;

    TrainsInputData* data = new TrainsInputData();
    data->Alpha =0;
    data->NumberOfTypes = 2;
    uint runningTimes[3] = { 0, 1, 2 };
    data->RunningTimes = runningTimes;
    uint numberOfTrainsPerType[3] = { 1, 1, 1 };
    data->NumberOfTrainsPerType = numberOfTrainsPerType;
    double trainDelays[9] = 
    { 0, 0, 0, 
    0, 1, 2, 
    0, 2, 3 };
    data->TrainDelays = trainDelays;

    BruteForceGenerator* generator = new BruteForceGenerator(data);

    Schedule* schedule = generator->Next();
    while (schedule != nullptr)
    {
        cout << schedule->ToString();
        cout << endl << endl;
        schedule = generator->Next();
    }
}

void Given_SimpleInput_When_CalculateConfigurationsCalled_Then_CorrectValueReturned()
{
    cout << "Given_SimpleInput_When_CalculateConfigurationsCalled_Then_CorrectValueReturned" << endl;

    // Arrange
    TrainsInputData* data = new TrainsInputData();
    data->Alpha = 1;
    data->NumberOfTypes = 2;
    uint runningTimes[3] = { 0, 1, 2 };
    data->RunningTimes = runningTimes;
    uint numberOfTrainsPerType[3] = { 1, 1, 1 };
    data->NumberOfTrainsPerType = numberOfTrainsPerType;
    double trainDelays[9] =
    { 0, 0, 0,
    0, 1, 2,
    0, 2, 3 };
    data->TrainDelays = trainDelays;

    // Act
    cout << CalculateConfiguration(data);

    //? Assert
}

int main(int argc, char* argv[])
{
    Given_SimpleGraph_When_RunningTimeCalled_Then_ReturnedValueIsCorrect();
    Given_NotSoSimpleGraph_When_RunningTimeCalled_Then_ReturnedValueIsCorrect();
    Given_SimpleMatrix_When_DeterminantCalculated_Then_ReturnedValueIsCorrect();
    Given_SimpleInput_When_BrutForceGeneratorCalled_Then_AllGraphsAreGenerated();
    Given_SimpleInput_When_CalculateConfigurationsCalled_Then_CorrectValueReturned();

    system("pause");
    return 0;
}
