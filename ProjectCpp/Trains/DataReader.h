#include <vector>
#include "Schedule.h"

#pragma once
class DataReader
{
public:
    DataReader(char* filePath);
    ~DataReader(void);

    std::vector<TrainsInputData*>* Data;
};

