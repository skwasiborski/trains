#include "ConfigurationCalculator.h"
#include "ScheduleGenerator.h"
#include "BruteForceGenerator.h"

struct ScheduleInfo
{
public:
    uint Cardinality;
    double RunningTime;

    bool operator<(const ScheduleInfo& other)
    {
        return RunningTime < other.RunningTime;
    }
};

double CalculateConfiguration(TrainsInputData* data)
{

    ScheduleGenerator* generator = new BruteForceGenerator(data);

    vector<ScheduleInfo*>* infos = new vector<ScheduleInfo*>();

    Schedule* schedule = generator->Next();
    do
    {
        ScheduleInfo* info = new ScheduleInfo();
        info->Cardinality = schedule->Cardinality();
        info->RunningTime = schedule->RunningTime();
        infos->push_back(info);
        schedule = generator->Next();
    } while (schedule != nullptr);

    sort(infos->begin(), infos->end());

    uint totalCardinality = 0;
    for (vector < ScheduleInfo*> ::size_type i = 0; i != infos->size(); i++)
    {
        totalCardinality += (*infos)[i]->Cardinality;
    }

    uint runningCardinality = 0;
    vector < ScheduleInfo*> ::size_type currentInfoIndex = 0;

    while ((double) runningCardinality / totalCardinality < data->Alpha)
    {
        runningCardinality += (*infos)[currentInfoIndex]->Cardinality;
    }

    uint result = (*infos)[currentInfoIndex]->RunningTime;

    for (vector < ScheduleInfo*> ::size_type i = 0; i != infos->size(); i++)
    {
        delete (*infos)[i];
    }

    return result;
}