#include "Schedule.h"

Schedule::Schedule(TrainsInputData* data, int kirhoffMatrix [], uint size)
{
    _data = data;
    _kirhoffMatrix = kirhoffMatrix;
    _kirhoffMatrixSize = size;
    _kirhoffMatrixSizeSq = _kirhoffMatrixSize * _kirhoffMatrixSize;
    _mec = nullptr;
}

Schedule::~Schedule(void)
{
    if (nullptr != _mec)
    {
        delete [] _mec;
    }
}

std::string Schedule::ToString()
{
    std::string result("");

    for (int i = 0; i < _kirhoffMatrixSize; i++)
    {
        for (int j = 0; j < _kirhoffMatrixSize; j++)
        {
            result += std::to_string(_kirhoffMatrix[i*_kirhoffMatrixSize + j]);
            result += ",\t";
        }
        result += "\n";
    }

    return result;
}

bool Schedule::IsConnected()
{
    // state: White = 0, Gray = 1, Black = 2
    int *state = new int[_kirhoffMatrixSize];
    for (int i = 0; i < _kirhoffMatrixSize; i++)
    {
        state[i] = 0;
    }

    runDFS(0, state);

    bool isConnected = true;

    for (int i = 0; i < _kirhoffMatrixSize; i++)
    {
        if (state[i] == 0)
        {
            isConnected = false;
            break;
        }
    }

    delete [] state;

    return isConnected;
}

void Schedule::runDFS(int u, int state [])
{
    state[u] = 1;
    for (int v = 0; v < _kirhoffMatrixSize; v++)
    {
        if (_kirhoffMatrix[u*_kirhoffMatrixSize + v] != 0 && state[v] == 0)
        {
            runDFS(v, state);
        }
    }

    state[u] = 2;
}

uint Schedule::Cardinality()
{
    //! TODO consider start condition
    uint det = this->Minor1Determinant();

    if (nullptr == _mec) { MinimalEulerCycle(); }

    ulong productOfFactorialOfDegG = 1;
    for (uint i = 1; i < _data->NumberOfTypes + 1; i++)
    {
        productOfFactorialOfDegG *= Factorial(Deg(i) - 1);
    }

    ulong enumerator = det * productOfFactorialOfDegG;
    ulong denominator = 1;

    for (uint i = 1; i < _data->NumberOfTypes + 1; i++)
    {
        for (uint j = 1; j < _data->NumberOfTypes + 1; j++)
        {
            if (i == j)
            {
                continue;
            }

            denominator *= Deleta(i, j);
        }
    }

    denominator = Factorial(denominator);

    ulong secoundCoeficient = 1;

    for (uint i = 1; i < _data->NumberOfTypes + 1; i++)
    {
        secoundCoeficient *=
            Factorial(_data->NumberOfTrainsPerType[i]) *
            Choose(_data->NumberOfTrainsPerType[i] - 1, Deg(i) - 1);
    }

    return enumerator / denominator * secoundCoeficient;
}

// TODO cache results
ulong Schedule::Choose(uint n, uint k)
{
    ulong* c = new ulong[k + 1];
    c[0] = 1;
    for (int i = 0; i < k; ++i)
    {
        c[i + 1] = (c[i] * (n - i)) / (i + 1);
    }

    return c[k];
}

// TODO: calculate all values in one loop
uint Schedule::Deleta(uint i, uint j)
{
    uint delta = 0;
    for (uint k = 0; k < _mecLength - 1; k++)
    {
        if (_mec[k] == i && _mec[k + 1] == j)
        {
            delta++;
        }
    }

    return delta;
}


int Schedule::Minor1Determinant()
{
    uint n = _kirhoffMatrixSize - 1;
    double* a = new double[n*n];
    for (uint i = 0; i < n; i++)
    {
        for (uint j = 0; j < n; j++)
        {
            a[i*n + j] = _kirhoffMatrix[i*_kirhoffMatrixSize + j];
        }
    }

    int det = this->Determinant(a, n);
    delete [] a;
    return det;
}

uint Schedule::Deg(uint i)
{
    uint deg = 0;
    for (uint j = 0; j < _kirhoffMatrixSize; j++)
    {
        if (i == j)
        {
            continue;
        }

        deg -= _kirhoffMatrix[i * _kirhoffMatrixSize + j];
    }

    return deg;
}

// TODO chache results.
ulong Schedule::Factorial(uint i)
{
    if (i == 0)
        return 1;
    ulong factorial = i;
    for (uint n = i - 1; n > 0; factorial *= n, --n);

    return factorial;
}

double Schedule::RunningTime()
{
    // b[i] = number of blocks of type i. Initialized with zeros
    uint* b;
    b = new uint[_data->NumberOfTypes + 1];
    for (uint i = 0; i < _data->NumberOfTypes + 1; i++) { b[i] = 0; }

    if (nullptr == _mec) { MinimalEulerCycle(); }

    double runningTime = _data->RunningTimes[_mec[_mecLength - 1]];

    // skip vertex at the beginning (it is of type 0)
    for (uint i = 1; i < _mecLength - 1; i++)
    {
        b[_mec[i]]++;
        runningTime += _data->TrainDelays[_mec[i] * _kirhoffMatrixSize + _mec[i + 1]];
    }

    b[_mec[_mecLength - 1]]++; // last element was not added in former loop

    // skip vertex at the beginning (it is of type 0)
    for (uint i = 1; i < _data->NumberOfTypes + 1; i++)
    {
        // blow up blocks to appropriate number of repetitions
        uint blowUpNumber = _data->NumberOfTrainsPerType[i] - b[i];
        if (blowUpNumber > 0)
        {
            runningTime += _data->TrainDelays[i * _kirhoffMatrixSize + i] * blowUpNumber;
        }
    }

    delete [] b;

    return runningTime;
}

void Schedule::MinimalEulerCycle()
{
    uint* miu = new uint[_kirhoffMatrixSizeSq];
    for (uint i = 0; i < _kirhoffMatrixSize; i++)
    {
        int rowSum = 0;
        for (uint j = 0; j < _kirhoffMatrixSize; j++)
        {
            if (i != j) rowSum -= _kirhoffMatrix[j*_kirhoffMatrixSize + i];
            miu[j*_kirhoffMatrixSize + i] = -_kirhoffMatrix[j*_kirhoffMatrixSize + i];
        }

        miu[i*_kirhoffMatrixSize + i] = rowSum - miu[i*_kirhoffMatrixSize + i];
    }

    uint u, v;
    u = v = 0;
    _mec = new uint[_kirhoffMatrixSizeSq];
    _mecLength = 0;
    do
    {
        _mec[_mecLength++] = u;
        u = this->MinimalNeighbour(v, miu);
        miu[v * _kirhoffMatrixSize + u]--;
        v = u;
    } while (v != 0);

    delete [] miu;
}

uint Schedule::MinimalNeighbour(uint u, uint* miu)
{
    _num = 0;
    _dfsnum = new uint[_kirhoffMatrixSize];
    _low = new uint[_kirhoffMatrixSize];

    for (uint i = 0; i < _kirhoffMatrixSize; i++) { _dfsnum[i] = -1; }
    for (uint i = 0; i < _kirhoffMatrixSize; i++) { _low[i] = -1; }

    uint result;
    result = this->MinimalNeighbourCore(u, miu);

    delete [] _dfsnum;
    delete [] _low;

    return result;
}

uint Schedule::MinimalNeighbourCore(uint u, uint* miu)
{
    _low[u] = _dfsnum[u] = _num++;
    uint minimalNeighbour = -1;
    for (uint v = 0; v < _kirhoffMatrixSize; ++v)
    {
        if (miu[u * _kirhoffMatrixSize + v] > 0 && _dfsnum[v] == -1)
        {
            if (minimalNeighbour == -1)
            {
                minimalNeighbour = v;
            }

            // (u, v) is a tree edge
            MinimalNeighbourCore(v, miu);
            if (_low[v] <= _dfsnum[u])
            {
                // this is minimal non bridge neghbour (thay have precedence to bridge neghbours)
                return v;
            }

            _low[u] = _low[u] < _low[v] ? _low[u] : _low[v];
        }
        else
        {
            // (u ,v) is a back edge
            _low[u] = _low[u] < _dfsnum[v] ? _low[u] : _dfsnum[v];
        }
    }

    // no non bridge neghbour found. return minimal neghbour.
    return minimalNeighbour;
}

uint Schedule::CroutLUDecomposition(double *A, uint n)
{
    uint i, j, k, p;
    double *p_k, *p_row, *p_col;

    //         For each row and column, k = 0, ..., n-1,
    //            find the lower triangular matrix elements for column k
    //            and if the matrix is non-singular (nonzero diagonal element).
    //            find the upper triangular matrix elements for row k. 

    for (k = 0, p_k = A; k < n; p_k += n, k++)
    {
        for (i = k, p_row = p_k; i < n; p_row += n, i++)
        {
            for (p = 0, p_col = A; p < k; p_col += n, p++)
            {
                *(p_row + k) -= *(p_row + p) * *(p_col + k);
            }
        }

        if (*(p_k + k) == 0)
        {
            return -1;
        }

        for (j = k + 1; j < n; j++)
        {
            for (p = 0, p_col = A; p < k; p_col += n, p++)
            {
                *(p_k + j) -= *(p_k + p) * *(p_col + j);
            }

            *(p_k + j) /= *(p_k + k);
        }
    }

    return 0;
}

int Schedule::Determinant(double *A, uint n)
{
    this->CroutLUDecomposition(A, n);

    int det = 1;
    for (uint i = 0; i < n; i++)
    {
        det *= A[i*n + i];
    }

    return det;
}

int Schedule::Determinant()
{
    double* a = new double[_kirhoffMatrixSize*_kirhoffMatrixSize];
    for (uint i = 0; i < _kirhoffMatrixSize; i++)
    {
        for (uint j = 0; j < _kirhoffMatrixSize; j++)
        {
            a[i*_kirhoffMatrixSize + j] = _kirhoffMatrix[i*_kirhoffMatrixSize + j];
        }
    }

    int det = this->Determinant(a, _kirhoffMatrixSize);
    delete [] a;
    return det;
}